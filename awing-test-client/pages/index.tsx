import type {NextPage} from 'next'
import Head from 'next/head'
import {Button, Form, Input, Select, Tabs} from "antd"
import {useEffect, useState} from "react";
import {Controller, SubmitHandler, useFieldArray, useForm, useWatch} from "react-hook-form";
import * as yup from 'yup'
import {yupResolver} from '@hookform/resolvers/yup'

interface IFormInputs {
    template?: string,
    email?: string,
    age?: string,
    gender?: string,
    id?: string,
    username?: string,
    password?: string,
}

interface Inputs {
    name?: string,
    title?: string,
    views:
        {
            value?: string,
            formInput?: IFormInputs
        }[]
}


const schema = yup.object().shape({
    name: yup.string().required('Required'),
    title: yup.string().required('Required'),
    views: yup
        .array()
        .of(
            yup.object().shape({
                formInput: yup.object().shape({
                    template: yup.string().required('Required'),
                    email: yup
                        .string()
                        .email('Must be a valid email')
                        .when("template", {
                            is: ((value: string) => value === 'template1'),
                            then: yup.string().required("Required")
                        }),
                    id: yup.string().when("template", {
                        is: ((value: string) => value === 'template2'),
                        then: yup.string().required("Required")
                    }),
                    username: yup.string().when("template", {
                        is: ((value: string) => value === 'template2'),
                        then: yup.string().required("Required")
                    }),
                })
            })
        )
        .nullable(),
})

const Home: NextPage = () => {
    const {TabPane} = Tabs
    const [steps, setSteps] = useState(['0', '1'])
    const {
        control,
        handleSubmit,
        setValue,
        getValues,
        reset,
        formState: {errors},
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
        defaultValues: {
            views: [{
                value: '0',
                formInput: {
                    template: '',
                    email: '',
                    age: '',
                    gender: '',
                    id: '',
                    username: '',
                    password: '',
                }
            }
            ]
        }
    })
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        console.log({data})
        alert(`Success`)
    }

    const {
        fields: viewFields,
        append: viewAppend,
        remove: viewRemove,
    } = useFieldArray({
        control,
        name: 'views',
    })
    const watchViews = useWatch({
        control,
        name: 'views',
    })
    useEffect(()=>{
       if(Object.entries(errors).length>0){
           alert('Please enter enough information!')
       }
    },[errors])
    return (
        <div className="flex min-h-screen flex-col items-center justify-center py-2">
            <Head>
                <title>Awing Basic Demo</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <main className="flex w-full flex-1 flex-col items-center justify-center px-20">
                <h1 className="text-6xl font-bold mb-20">
                    <a className="text-blue-600" href="#">
                        AWING{' '}
                    </a>
                    Basic demo
                </h1>
                <Form onFinish={handleSubmit(onSubmit)} className={'w-1/2'}>
                    <Tabs defaultActiveKey="0"
                          tabBarExtraContent={<><Button type={'primary'} htmlType={'submit'}>Submit</Button></>}>
                        {steps?.map((step) => {
                            if (step === '0') {
                                return (
                                    <TabPane tab={`Step ${step}`} key={step}>
                                        <Form
                                            labelCol={{span: 4}}
                                            wrapperCol={{span: 14}}
                                            layout="horizontal"
                                        >
                                            <Form.Item
                                                required={true}
                                                label={<span>Name</span>}
                                                validateStatus={errors?.name && 'error'}
                                                help={
                                                    errors?.name && errors?.name.message
                                                }
                                            >
                                                <Controller
                                                    control={control}
                                                    name={'name'}
                                                    render={({field}) => (
                                                        <Input
                                                            {...field}
                                                            size="middle"
                                                        />
                                                    )}
                                                />
                                            </Form.Item>
                                            <Form.Item
                                                required={true}
                                                label={<span>Title</span>}
                                                validateStatus={errors?.title && 'error'}
                                                help={
                                                    errors?.title && errors?.title.message
                                                }
                                            >
                                                <Controller
                                                    control={control}
                                                    name={'title'}
                                                    render={({field}) => (
                                                        <Input
                                                            {...field}
                                                            size="middle"
                                                        />
                                                    )}
                                                />
                                            </Form.Item>
                                        </Form>
                                    </TabPane>
                                )
                            } else if (step === '1') {
                                return (
                                    <TabPane tab={`Step ${step}`} key={step}>
                                        <Form
                                            labelCol={{span: 4}}
                                            wrapperCol={{span: 14}}
                                            layout="horizontal"
                                        >
                                            <Tabs defaultActiveKey="view-0" type="card"
                                                  tabBarExtraContent={<><Button type={'primary'} onClick={() => {
                                                      if (watchViews.find((view) => view?.formInput?.template === '')) {
                                                          alert('You have not selected a template')
                                                          return
                                                      }
                                                      if (watchViews.find((view) => view?.formInput?.template === 'template1' && view?.formInput?.email === '')) {
                                                          alert('You have not entered your email')
                                                          return
                                                      }
                                                      if (watchViews.find((view) => view?.formInput?.template === 'template2' && view?.formInput?.id === '' && view?.formInput?.username === '')) {
                                                          alert('You have not entered your id and username')
                                                          return
                                                      }
                                                      if (watchViews.length <= 1)
                                                          viewAppend({
                                                              value: '1',
                                                              formInput: {
                                                                  template: '',
                                                                  email: '',
                                                                  age: '',
                                                                  gender: '',
                                                                  id: '',
                                                                  username: '',
                                                                  password: '',
                                                              }
                                                          })
                                                      else {
                                                          const view = Number(watchViews[watchViews.length - 1].value) + 1
                                                          viewAppend({
                                                              value: view.toLocaleString(),
                                                              formInput: {
                                                                  template: '',
                                                                  email: '',
                                                                  age: '',
                                                                  gender: '',
                                                                  id: '',
                                                                  username: '',
                                                                  password: '',
                                                              }
                                                          })
                                                      }
                                                  }
                                                  }>+ Add new View</Button></>}>
                                                {viewFields?.map((field, index) => {
                                                    return (
                                                        <TabPane tab={`View ${field.value}`}
                                                                 key={`view-${field.value}`}>
                                                            <Form.Item
                                                                required={true}
                                                                label={<span>Template</span>}
                                                                validateStatus={
                                                                    errors.views?.[index]
                                                                        ?.formInput?.template && 'error'
                                                                }
                                                                help={
                                                                    errors.views?.[index]
                                                                        ?.formInput?.template?.message
                                                                }
                                                            >
                                                                <Controller
                                                                    control={control}
                                                                    name={`views.${index}.formInput.template`}
                                                                    render={({field}) => (
                                                                        <Select {...field}>
                                                                            <Select.Option
                                                                                value={''}>None</Select.Option>
                                                                            <Select.Option value={'template1'}>Template
                                                                                1</Select.Option>
                                                                            <Select.Option value={'template2'}>Template
                                                                                2</Select.Option>
                                                                        </Select>
                                                                    )}
                                                                />
                                                            </Form.Item>
                                                            {getValues(`views.${index}.formInput.template`) === 'template1' && (
                                                                <>
                                                                    <Form.Item
                                                                        required={true}
                                                                        label={<span>Email</span>}
                                                                        validateStatus={
                                                                            errors.views?.[index]
                                                                                ?.formInput?.email && 'error'
                                                                        }
                                                                        help={
                                                                            errors.views?.[index]
                                                                                ?.formInput?.email?.message
                                                                        }
                                                                    >
                                                                        <Controller
                                                                            control={control}
                                                                            name={`views.${index}.formInput.email`}
                                                                            render={({field}) => (
                                                                                <Input
                                                                                    {...field}
                                                                                    placeholder="Email"
                                                                                />
                                                                            )}
                                                                        />
                                                                    </Form.Item>
                                                                    <Form.Item
                                                                        label={<span>Age</span>}
                                                                    >
                                                                        <Controller
                                                                            control={control}
                                                                            name={`views.${index}.formInput.age`}
                                                                            render={({field}) => (
                                                                                <Input
                                                                                    {...field}
                                                                                    placeholder="Age"
                                                                                />
                                                                            )}
                                                                        />
                                                                    </Form.Item>
                                                                    <Form.Item
                                                                        label={<span>Gender</span>}
                                                                    >
                                                                        <Controller
                                                                            control={control}
                                                                            name={`views.${index}.formInput.gender`}
                                                                            render={({field}) => (
                                                                                <Input
                                                                                    {...field}
                                                                                    placeholder="Gender"
                                                                                />
                                                                            )}
                                                                        />
                                                                    </Form.Item>
                                                                </>
                                                            )
                                                            }
                                                            {getValues(`views.${index}.formInput.template`) === 'template2' && (
                                                                <>
                                                                    <Form.Item
                                                                        required={true}
                                                                        label={<span>Id</span>}
                                                                        validateStatus={
                                                                            errors.views?.[index]
                                                                                ?.formInput?.id && 'error'
                                                                        }
                                                                        help={
                                                                            errors.views?.[index]
                                                                                ?.formInput?.id?.message
                                                                        }
                                                                    >
                                                                        <Controller
                                                                            control={control}
                                                                            name={`views.${index}.formInput.id`}
                                                                            render={({field}) => (
                                                                                <Input
                                                                                    {...field}
                                                                                    placeholder="Id"
                                                                                />
                                                                            )}
                                                                        />
                                                                    </Form.Item>
                                                                    <Form.Item
                                                                        required={true}
                                                                        label={<span>Username</span>}
                                                                        validateStatus={
                                                                            errors.views?.[index]
                                                                                ?.formInput?.username && 'error'
                                                                        }
                                                                        help={
                                                                            errors.views?.[index]
                                                                                ?.formInput?.username?.message
                                                                        }
                                                                    >
                                                                        <Controller
                                                                            control={control}
                                                                            name={`views.${index}.formInput.username`}
                                                                            render={({field}) => (
                                                                                <Input
                                                                                    {...field}
                                                                                    placeholder="Username"
                                                                                />
                                                                            )}
                                                                        />
                                                                    </Form.Item>
                                                                    <Form.Item
                                                                        label={<span>Password</span>}
                                                                    >
                                                                        <Controller
                                                                            control={control}
                                                                            name={`views.${index}.formInput.password`}
                                                                            render={({field}) => (
                                                                                <Input
                                                                                    {...field}
                                                                                    placeholder="Password"
                                                                                />
                                                                            )}
                                                                        />
                                                                    </Form.Item>
                                                                </>
                                                            )
                                                            }
                                                        </TabPane>

                                                    )
                                                })
                                                }
                                            </Tabs>
                                        </Form>
                                    </TabPane>
                                )
                            }
                        })
                        }
                    </Tabs>
                </Form>
            </main>

            <footer className="flex h-24 w-full items-center justify-center border-t">
                <p
                    className="flex items-center justify-center gap-2"
                >
                    Developed by{' '} <b>TienNP</b>

                </p>
            </footer>
        </div>
    )
}

export default Home
